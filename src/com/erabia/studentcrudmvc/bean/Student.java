package com.erabia.studentcrudmvc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "STUDENTS")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "FirstName")
	@NotNull
	@Size(min=1,message="first name cannot be less than 1 character")
	private String firstName;
	@Column(name = "LastName")
	@NotNull
	@Size(min=1,message="last name cannot be less than 1 character")
	private String lastName;
	@Column(name = "Avg")
	@NotNull
	@Min(value=0,message="avg cannot be less than zero")
	@Max(value=100,message = "avg cannot be more than 100")
	private double avg;

	public Student() {
	}

	public Student(String firstName, String lastName, double avg) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.avg = avg;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", avg=" + avg + "]";
	}

}
