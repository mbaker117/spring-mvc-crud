package com.erabia.studentcrudmvc;

import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.erabia.studentcrudmvc.bean.Student;
import com.erabia.studentcrudmvc.dao.StudentDao;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-servlet.xml");
		 StudentDao bean = context.getBean("studentDao",StudentDao.class);
		 	Optional<Student> optional = bean.get(77);
		System.out.println(optional.get().toString());
		System.out.println("end!!");
	}

}
