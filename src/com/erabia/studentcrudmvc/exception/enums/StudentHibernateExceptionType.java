package com.erabia.studentcrudmvc.exception.enums;

public enum StudentHibernateExceptionType {

	MISSING_CONNECTION("connection can't be established."), EXISTING_STUDENT("student already exists"),
	NO_EXISTING_STUDENT("student not found"), INVALID_AVG("student avg is invalid"),
	CLASS_NOT_FOUND("class not found in connection"), SQL_EXCEPTION("error in sql"), IO_EXCEPTION("error in io"),
	NO_SUCH_METHOD_EXCEPTION("no method exception"), SECURITY_EXCEPTION("security exception"),
	INSTANTIATION_EXCEPTION("instantiation exception"), ILLEGAL_ACCESS_EXCEPTION("illegal access exception"),
	ILLEGAL_ARGUMENT_EXCEPTION("illegal argument exception"),
	INVOCATION_TARGET_EXCEPTION("invocation target exception"),NO_STUDENTS("table is empty");

	private String msg;

	private StudentHibernateExceptionType(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}
}
