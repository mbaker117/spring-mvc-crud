package com.erabia.studentcrudmvc.exception;

import com.erabia.studentcrudmvc.exception.enums.StudentHibernateExceptionType;

public class StudentHibernateException  extends Exception{
	private final StudentHibernateExceptionType type;
	private final String value;
	public StudentHibernateException(final StudentHibernateExceptionType type, final String message,final String value) {
		super(message);
		this.type = type;
		this.value = value;
	}
	public StudentHibernateExceptionType getType() {
		return type;
	}
	public String getValue() {
		return value;
	}
	
	

}
