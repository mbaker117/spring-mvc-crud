package com.erabia.studentcrudmvc.service.impl;

import java.util.List;
import java.util.Optional;

import com.erabia.studentcrudmvc.bean.Student;
import com.erabia.studentcrudmvc.dao.StudentDao;
import com.erabia.studentcrudmvc.exception.StudentHibernateException;
import com.erabia.studentcrudmvc.service.StudentService;

public class StudentServiceImpl implements StudentService {
	private StudentDao studentDao;
	
	
	public StudentServiceImpl(StudentDao studentDao) {
		super();
		this.studentDao = studentDao;
	}
	

	public final StudentDao getStudentDao() {
		return studentDao;
	}


	public final void setStudentDao(StudentDao studentDao) {
		this.studentDao = studentDao;
	}


	@Override
	public Student add(Student student) {
		// TODO Auto-generated method stub
		return studentDao.add(student);
	}

	@Override
	public Student update(Student student) {
		// TODO Auto-generated method stub
		return studentDao.update(student);
	}

	@Override
	public void delete(int id) throws StudentHibernateException {
		// TODO Auto-generated method stub
		studentDao.delete(id);
	}

	@Override
	public Optional<Student> get(int id) throws StudentHibernateException {
		// TODO Auto-generated method stub
		return studentDao.get(id);
	}

	@Override
	public Optional<List<Student>> getAll() {
		// TODO Auto-generated method stub
		return studentDao.getAll();
	}

}
