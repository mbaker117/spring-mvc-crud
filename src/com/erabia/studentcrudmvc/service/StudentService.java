package com.erabia.studentcrudmvc.service;

import java.util.List;
import java.util.Optional;

import com.erabia.studentcrudmvc.bean.Student;
import com.erabia.studentcrudmvc.exception.StudentHibernateException;



public interface StudentService {
	public Student add(Student student) ;

	public Student update( Student student);

	public void delete(int id) throws StudentHibernateException;

	public Optional<Student> get(int id) throws StudentHibernateException;

	public Optional<List<Student>> getAll() ;
}
