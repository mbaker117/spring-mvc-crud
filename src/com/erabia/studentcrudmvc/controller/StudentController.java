package com.erabia.studentcrudmvc.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.erabia.studentcrudmvc.bean.Student;
import com.erabia.studentcrudmvc.exception.StudentHibernateException;
import com.erabia.studentcrudmvc.service.StudentService;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	public StudentController(StudentService studentService) {
		super();
		this.studentService = studentService;
	}
	
	public  StudentService getStudentService() {
		return studentService;
	}
	public  void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}

	private StudentService studentService;
	@GetMapping("/viewStudent")
	public String viewStudent(Model model) {
		Optional<List<Student>> all = studentService.getAll();
		model.addAttribute("students",all.get());
		return "studentPage";
	}
	@GetMapping("/addStudentForm")
	public String addStudentForm(Model model) {
		model.addAttribute("student",new Student());
		
		return "studentForm";
	}
	@PostMapping("saveStudent")
	public String saveStudent(@Valid @ModelAttribute("student") Student student,BindingResult bindingResult) {
		System.out.println(bindingResult);
		if(bindingResult.hasErrors())
			return "studentForm";
		if(student.getId()==0)
			studentService.add(student);
		else
			studentService.update(student);
		
		return "redirect:/student/viewStudent";
	}
	@GetMapping("editStudent")
	public String editStudent(@RequestParam("studentId")int id ,Model model) throws StudentHibernateException {
		
		Optional<Student> optional = studentService.get(id);
		model.addAttribute("student", optional.get());
		return "studentForm";
	}
	@GetMapping("/deleteStudent")
	public String deleteStudent(@RequestParam("studentId")int id) throws StudentHibernateException {
		studentService.delete(id);
		return "redirect:/student/viewStudent";
	}
	@InitBinder 
	public void validate(WebDataBinder webDataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		webDataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}
}
