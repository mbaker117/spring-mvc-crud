package com.erabia.studentcrudmvc.dao;

import java.util.List;
import java.util.Optional;

import com.erabia.studentcrudmvc.bean.Student;
import com.erabia.studentcrudmvc.exception.StudentHibernateException;

public interface StudentDao {
	public Student add(Student student);
	public Student update(Student student);
	public void delete(int id) throws StudentHibernateException;
	public Optional<Student> get(int id);
	public Optional<List<Student>> getAll();
}
