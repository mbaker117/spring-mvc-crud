<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Student Page</h2>
	<a href="addStudentForm">Add Student</a>
	<br>
	<br>
		<a href="${pageContext.request.contextPath}/logout">Logout</a>
	<br>
	<br>
	<table border="1" width="90%">
		<tr>
			<th>Id</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Avg</th>

			<th>Edit</th>
			<th>Delete</th>
		</tr>

		<c:forEach var="std" items="${students}">
			<tr>
				<c:url var="deleteLink" value="/student/deleteStudent">
					<c:param name="studentId" value="${std.id }"></c:param>
				</c:url>
				<c:url var="editLink" value="/student/editStudent">
					<c:param name="studentId" value="${std.id }"></c:param>
				</c:url>
				<td>${std.id}</td>
				<td>${ std.firstName}</td>
				<td>${std.lastName}</td>
				<td>${std.avg}</td>




				<td><a
					href="${editLink }">Edit</a></td>
				<td><a
					href="${deleteLink}">Delete</a></td>
			</tr>

		</c:forEach>






	</table>
</body>
</html>