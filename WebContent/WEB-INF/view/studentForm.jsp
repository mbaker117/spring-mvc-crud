<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.error {
	color: red
}
</style>
</head>
<body>
	<h2>Save Student</h2>
	<form:form action="saveStudent" method="post" modelAttribute="student">
		<form:hidden path="id" />
		FirstName: <form:input path="firstName" />
		<form:errors path="firstName" cssClass="error"/>

		<br>
		<br>
		LastName: <form:input path="lastName" />
		<form:errors path="lastName" cssClass="error"/>
		<br>
		<br>
		Avg: <form:input path="avg" />
		<form:errors path="avg" cssClass="error"/>
		<br>
		<br>
		<input type="submit" value="save" />


	</form:form>
	<br>
	<a href="viewStudent">View Student</a>
</body>
</html>